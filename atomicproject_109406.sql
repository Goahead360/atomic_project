-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 11, 2016 at 03:59 AM
-- Server version: 10.0.17-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomicproject_107095`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE `birthday` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `birthdate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `username`, `birthdate`) VALUES
(4, 'sulab_novel', '1989-01-26'),
(5, 'fgdfgdfgdg', '1996-12-25'),
(6, 'gdssd', '2000-05-14'),
(9, 'sdfsdf', '2008-07-25'),
(11, 'sadasdas', '1996-12-02'),
(12, 'dsada', '2005-07-25'),
(13, 'asdas', '1990-07-15'),
(14, 'sulab saha', '0000-00-00'),
(15, 'erwrwer', '1990-07-14');

-- --------------------------------------------------------

--
-- Table structure for table `booktitles`
--

CREATE TABLE `booktitles` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `author` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booktitles`
--

INSERT INTO `booktitles` (`id`, `title`, `author`) VALUES
(49, 'dfgdfgdfgdfg', 'dgdrtertertert');

-- --------------------------------------------------------

--
-- Table structure for table `citynames`
--

CREATE TABLE `citynames` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `cityname` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `citynames`
--

INSERT INTO `citynames` (`id`, `username`, `cityname`) VALUES
(1, 'hhsulab', 'Dhaka'),
(2, 'sudip', 'Chittagong'),
(3, 'shymal', 'Chittagong'),
(4, 'sdas', 'Khulna'),
(5, 'sujits', 'Barishal'),
(6, 'monir', 'Shylet');

-- --------------------------------------------------------

--
-- Table structure for table `conditions`
--

CREATE TABLE `conditions` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `criteria` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `conditions`
--

INSERT INTO `conditions` (`id`, `username`, `criteria`) VALUES
(1, 'sulab saha', 'Agree'),
(3, 'fsfsdf', 'Disagree');

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

CREATE TABLE `emails` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emails`
--

INSERT INTO `emails` (`id`, `username`, `email`) VALUES
(1, 'Pial Ahmed', 'dsjhgfdsjhfgs@yahoo.com'),
(2, 'sudip saha', 'fdsdshg@gmail.com'),
(3, 'Rahim', 'sdashd@yahoo.com'),
(4, 'Himel ', 'dhgdhgh@yahoo.com');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `username`, `type`) VALUES
(3, 'Sulab Saha', 'Male'),
(4, 'Riad Hossen', 'Male'),
(5, 'Riad Hossen', 'Male'),
(6, 'Farhana Sultan', 'Female'),
(7, 'dfsf', 'Female'),
(8, 'werwr', 'Female'),
(9, 'weqqe', 'Male');

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE `hobbies` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `hobby` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `username`, `hobby`) VALUES
(10, 'sfsfs', 'Painting, Watching Movies'),
(11, 'saasdasd', 'Reading, Watching Movies'),
(12, 'sudip saha', 'Listening to Music, Painting'),
(13, 'sdadad', 'Watching Movies,Traveling'),
(14, 'erwrwre', 'Reading,Listening to Music');

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `picture` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`id`, `username`, `picture`) VALUES
(35, 'ssssssssssssssssssssss', 0x66696c652f5048505f4c6f676f2e706e67);

-- --------------------------------------------------------

--
-- Table structure for table `summary`
--

CREATE TABLE `summary` (
  `id` int(11) NOT NULL,
  `organization` varchar(50) NOT NULL,
  `summary` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summary`
--

INSERT INTO `summary` (`id`, `organization`, `summary`) VALUES
(2, 'Beximco', ' The BEXIMCO name has now become one of the most recognizable brand names in Bangladesh. It is synonymous with innovation, trust and quality. The Group consists of four publicly traded and seventeen dasdadadasdasdas\r\n                                           \r\n                                         \r\n                    ');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booktitles`
--
ALTER TABLE `booktitles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `citynames`
--
ALTER TABLE `citynames`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conditions`
--
ALTER TABLE `conditions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emails`
--
ALTER TABLE `emails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summary`
--
ALTER TABLE `summary`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `booktitles`
--
ALTER TABLE `booktitles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `citynames`
--
ALTER TABLE `citynames`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `conditions`
--
ALTER TABLE `conditions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `emails`
--
ALTER TABLE `emails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `profile`
--
ALTER TABLE `profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `summary`
--
ALTER TABLE `summary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
