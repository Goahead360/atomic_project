<?php

namespace App\Bitm\SEIP109406\Utility; 
session_start();
class Utility {
    
    
    public static $message;


    static public function d($param = false) {
        echo "<pre>";
        var_dump($param);
        echo "<pre>";
    }
    
    static public function dd($param = false) {
        self::d($param);
        die();
    }
    
    static public function redirect_booktitle($url="/atomicProject_109406/views/SEIP109406/Academic/index.php") {
        header("location:".$url);   
    }
     static public function redirect_hobby($url="/atomicProject_109406/views/SEIP109406/Leisure/index.php") {
        header("location:".$url);   
    }
     static public function redirect_email($url="/atomicProject_109406/views/SEIP109406/Subscription/index.php") {
        header("location:".$url);   
    }
    static public function redirect_city($url="/atomicProject_109406/views/SEIP109406/geographicalArea/index.php") {
        header("location:".$url);   
    }
    static public function redirect_summary($url="/atomicProject_109406/views/SEIP109406/Organization/index.php") {
        header("location:".$url);   
    }
    static public function redirect_gender($url="/atomicProject_109406/views/SEIP109406/Gender/index.php") {
        header("location:".$url);   
    }
    static public function redirect_birthday($url="/atomicProject_109406/views/SEIP109406/Date/index.php") {
        header("location:".$url);   
    }
    static public function redirect_picture($url="/atomicProject_109406/views/SEIP109406/Picture/index.php") {
        header("location:".$url);   
    }
    static public function redirect_condition($url="/atomicProject_109406/views/SEIP109406/Conditions/index.php") {
        header("location:".$url);   
    }
    
    
    static public function message($message = null){
        if(is_null($message)){ // please give me message
            $mes = self::getMessage();
            return $mes;
        }else{ //please set this message
            self::setMessage($message);
        }
    }
    
    static private function getMessage(){
        
        $mes =  $_SESSION['message'];
        $_SESSION['message'] = "";
        return $mes;
    }
    
    static private function setMessage($message){
        $_SESSION['message'] = $message;
    }
}
