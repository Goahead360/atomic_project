<!DOCTYPE html>

<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Book Title</title>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <link rel="stylesheet" href="../../../resource/css/bootstrap.min.css">
        <script src="../../../resource/js/jquery.min.js"></script>
        <script src="../../../resource/js/bootstrap.min.js"></script>

    </head>
    <body>
        <br><br>
        <div class="container">
            <form role="form" action="store.php" method="post">
            <fieldset class="form-group">
                <legend>Add Book Title</legend>
                
                <div class="form-group">
                    <label for="title">Enter Book Title: </label>
                    <input placeholder="Enter Book Title"
                           class="form-control"
                           type="text"
                           name="title"
                           id="title"
                           autofocus="autofocus"
                           tabindex="1"
                           required="required"
                           size="50"/>
                   
                    <label for="author">Enter Author Name: </label>
                    <input placeholder="Enter Book Title"
                           class="form-control"
                           type="text"
                           name="author"
                           id="author"
                           autofocus="autofocus"
                           tabindex="1"
                           required="required"
                           size="50"/>
                   
                </div>
                
                <div class="form-group">
                    <button tabindex="2"type="Submit" class="btn btn-info">Save</button>
                    <button tabindex="3"type="Submit" class="btn btn-info">Save & Add</button>
                    <button tabindex="4"type="reset" class="btn btn-info">Reset</button>
                
                </div>
            </fieldset>    
        </form>
        
            <nav>
                <li><a href="index.php">Go To List</a></li>
                <li><a href="javascript:history.go(-1)">Back</a></li>
            </nav>
        </div>    
    </body>
</html>
