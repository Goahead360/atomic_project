<?php
        include_once ("../../../vendor/autoload.php");
        use \App\Bitm\SEIP109406\Conditions\Terms;
        
        
        
        $condition = new Terms();       
        $conditions = $condition->index();

        ?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Terms & Conditions</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../../../resource/css/bootstrap.min.css">
        <script src="../../../resource/js/jquery.min.js"></script>
        <script src="../../../resource/js/bootstrap.min.js"></script>
        <style>
            #utility{
                float: right;
                width: 30%;
            }
        </style>

    </head>
    <body>
        <div class="container"><br>
        <a href="../../../index.php"><button type="button" class="btn btn-primary">Project Homepage</button></a>   
        <h1>Acceptance of Terms & Conditions</h1>
        
        <div><span id="utility">Download as PDF | XL | <a href="create.php"><button type="button" class="btn btn-info">Add New</button></a></span></div>
        
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Serial No.</th>
                    <th>User Name</th>
                    <th>Acceptance Criteria</th>
                    <th>Actions</th>
                </tr>
            </thead>
            
            <tbody>
                <?php
                $sl = 1;
                foreach ($conditions as $condition){
                    
                
                ?>
                <tr>
                    <td><?php echo $sl;?></td>
                    <td><?php echo $condition['username'];?></td>
                    <td><?php echo $condition['criteria'];?></td>
                    <td>Edit | View | Delete | Trash/Recover | Email to Friend</td>
                </tr>
                <?php
                $sl++;
                }
                
                ?>
                
            </tbody>
        </table>
        </div>
     <div><span id="utility">prev 1 | 2 | 3 next</span></div>           
         
    </body>
</html>
