<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Edit Profile Picture</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../../../resource/css/bootstrap.min.css">
        <link rel="stylesheet" href="../../../resource/css/fileinput.min.css">
        <script src="../../../resource/js/jquery.min.js"></script>
        <script src="../../../resource/js/bootstrap.min.js"></script>
        <script src="../../../resource/js/fileinput.min.js"></script>
        <script src="../../../resource/js/canvas-to-blob.min.js"></script>

        
        
    </head>
    <body>
        <br><br>  
          <div class="container">
              <div class="btn-group">
                  <a href="index.php"><button type="button" class="btn btn-primary">Home</button></a>
                  <a href="store.php"><button type="button" class="btn btn-primary">Store</button></a>
                  <a href="create.php"><button type="button" class="btn btn-primary">Create</button></a>
                  <a href="update.php"><button type="button" class="btn btn-primary">Update</button></a>
                  <a href="delete.php"><button type="button" class="btn btn-primary">Delete</button></a>
  </div>

              <br><br><br><br>
              <h2 style="text-align: center">Edit Profile Picture</h2>
  <br><br><br><br><br>
  <form role="form">
    <div class="form-group">
          <label class="control-label">Change your Profile Picture</label>
    <input id="input-1" type="file" class="file">
    </div>
      <br><br><br><br><br>
      
    
    <div class="form-actions">
            <center>
                    <button type="submit" class="btn btn-info btn-lg ">
                        Click for Submit <i class="icon-angle-right"></i>
                    </button>
            </center>
</div>
  </form>
          </div>
        
    </body>
</html>
