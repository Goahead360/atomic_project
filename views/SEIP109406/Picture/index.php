<?php
        include_once ("../../../vendor/autoload.php");
        use \App\Bitm\SEIP109406\Picture\ProfilePicture;
        
        
        
        $profile = new ProfilePicture();
        
        $pictures = $profile->index();
        
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Profile Picture</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../../../resource/css/bootstrap.min.css">
        <script src="../../../resource/js/jquery.min.js"></script>
        <script src="../../../resource/js/bootstrap.min.js"></script>

    </head>
    <body>
        <div class="container"><br>
        <a href="../../../index.php"><button type="button" class="btn btn-primary">Project Homepage</button></a>    
        <h1>My Profile Picture</h1>
        <div><span id="utility"><a href="create.php"><button type="button" class="btn btn-info">Add New</button></a></span></div>
        
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Serial No.</th>
                    <th>Username</th>
                    <th>Profile Picture</th>
                    <th>Actions</th>
                </tr>
            </thead>
            
            <tbody>
                <?php
                $sl = 1;
                foreach ($pictures as $picture){
                    
                
                ?>
                <tr>
                    <td><?php echo $sl;?></td>
                    <td><?php echo $picture['username'];?></td>
                    <td><img src="<?php $picture['picture']?>"/></td>
                    <td>Edit | Delete | Trash/Recover | Email to Friend</td>
                </tr>
                <?php
                $sl++;
                }
                ?>





                
            </tbody>
        </table>
        </div>
        
    </body>
</html>
