<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Add E-mail </title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../../../resource/css/bootstrap.min.css">
        <script src="../../../resource/js/jquery.min.js"></script>
        <script src="../../../resource/js/bootstrap.min.js"></script>

    </head>
    <body>
        <br><br>
        <div class="container">
            <form role="form" action="store.php" method="post">
            <fieldset class="form-group">
                <legend>Add E-mail</legend>
                
                <div class="form-group">
                    <label for="username">Enter Your Name : </label>
                    <input placeholder="Enter your name "
                           class="form-control"
                           type="text"
                           name="username"
                           id="username"
                           autofocus="autofocus"
                           tabindex="1"
                           required="required"
                           size="50"/>
                </div>
                <div class="form-group">
                    <label for="email">Enter Your E-mail : </label>
                    <input placeholder="Enter your mail address"
                           class="form-control"
                           type="email"
                           name="email"
                           id="email"
                           autofocus="autofocus"
                           tabindex="1"
                           required="required"
                           size="50"/>
                </div>
                
                <div class="form-group">
                    <button tabindex="2"type="Submit" class="btn btn-info">Save</button>
                    <button tabindex="3"type="Submit" class="btn btn-info">Save & Add</button>
                    <button tabindex="4"type="reset" class="btn btn-info">Reset</button>
                
                </div>
            </fieldset>    
        </form>
        
            <nav>
                <li><a href="index.php">Go To List</a></li>
                <li><a href="javascript:history.go(-1)">Back</a></li>
            </nav>
        </div>  
    </body>
</html>
